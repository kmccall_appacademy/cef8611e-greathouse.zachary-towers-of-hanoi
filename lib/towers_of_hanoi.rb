# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [ [3,2,1], [], [] ]
  end

  def move(from_tower, to_tower)
    disc = towers[from_tower].pop
    towers[to_tower].push(disc)
  end

  def valid_move?(from_tower, to_tower)
    from = @towers[from_tower]
    to = @towers[to_tower]

    if !to.empty?
      if from.empty? || to.last < from.last
        return false
      elsif from.last < to.last
        return true
      end
    else
      return true
    end

  end

  def won?
    @towers[1].length == 3 || @towers[2].length == 3 ? true : false
  end

  def play

    until won?
      puts render
      make_move
    end

    game_over
  end

  def make_move
    print "Please make your move: (ex: from to)"
    input = gets.chomp
    from_tower, to_tower = input.split(" ").map(&:to_i)

    until valid_move?(from_tower, to_tower)
      from_tower, to_tower = get_valid_move
    end

    move(from_tower, to_tower)
  end

  def get_valid_move
    print "Please enter a valid move: "
    input = gets.chomp

    input.split(" ").map(&:to_i)
  end

  def disc(num)
    if num == 1
      "  [=]  "
    elsif num == 2
      " [===] "
    elsif num == 3
      "[=====]"
    end
  end


  def render
    disc_one =   "  [=]  "
    disc_two =   " [===] "
    disc_three = "[=====]"

    top_row = towers.map do
      |tower| tower.length >= 3 ? disc(tower[2]) : '       '
    end

    mid_row = towers.map do
      |tower| tower.length >= 2 ? disc(tower[1]) : '       '
    end

    low_row = towers.map do |tower|
      tower.length >= 1 ? disc(tower[0]) : '       '
    end

    puts "#{top_row.join(" ")}"
    puts "#{mid_row.join(" ")}"
    puts "#{low_row.join(" ")}"
    puts "------- ------- -------"
  end

  def game_over
    puts "You win! Thank you for playing!"
    render
  end

end
